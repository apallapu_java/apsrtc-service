package com.poc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poc.model.Ticket;
import com.poc.repository.ApsrtcReposiotry;

@Service
public class ApsrtcService {

	@Autowired
	ApsrtcReposiotry apsrtcReposiotry;

	public Ticket createTicket(Ticket ticket) {
		return apsrtcReposiotry.save(ticket);
	}

	public Ticket updateTicket(Ticket ticket, Long id) {
		Ticket updatedTicket = apsrtcReposiotry.findById(id).orElse(null);
		updatedTicket.setCost(ticket.getCost());
		updatedTicket.setName(ticket.getName());
		updatedTicket.setTicket_from(ticket.getTicket_from());
		updatedTicket.setTicket_to(ticket.getTicket_to());

		return apsrtcReposiotry.save(updatedTicket);
	}

	public Ticket getTicketById(Long id) {
		return apsrtcReposiotry.findById(id).orElse(null);
	}

	public List<Ticket> getAllTickets() {

		return apsrtcReposiotry.findAll();
	}

	public void deleteTicketById(Long id) {
		apsrtcReposiotry.deleteById(id);
	}

}
